require recipes-core/images/core-image-minimal.bb

IMAGE_FEATURES_append = " \
    ssh-server-dropbear \
    "

IMAGE_INSTALL_append = " \
    gdb \
    gdbserver \
    openssh-sftp-server \
    tcf-agent \
    "
