inherit kernel
require recipes-kernel/linux/linux-yocto.inc

FILESEXTRAPATHS_prepend = "${THISDIR}/linux-socfpga:"

LINUX_VERSION = "4.18"
LINUX_VERSION_EXTENSION_append = "-socfpga"

SRC_URI = "git://github.com/altera-opensource/linux-socfpga.git;protocol=git;branch=socfpga-4.18;name=machine"
SRCREV_machine = "424c9e80343f3a81b18eee98edc36b7c576a3b7d"
LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

SRC_URI_append += " file://defconfig"
KCONFIG_MODE = "alldefconfig"

PV = "${LINUX_VERSION}+git${SRCPV}"

COMPATIBLE_MACHINE = "(arria10)"
